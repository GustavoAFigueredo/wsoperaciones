package com.cyxtera.wsOperaciones.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;



@Service
public class OperandoService {

	
	public ArrayList<Double> operandos = new ArrayList<>();
	
	public void addOperando(String operando) {
		Double valor = Double.parseDouble(operando);
		operandos.add(valor);
	}
	
	public ArrayList<Double> getOperandos(){
		return operandos;
	}
	
	public void limpiaOperandos(){
		operandos.clear();
	}
	

}
