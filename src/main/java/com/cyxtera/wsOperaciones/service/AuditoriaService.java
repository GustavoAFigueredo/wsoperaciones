package com.cyxtera.wsOperaciones.service;

import java.util.List;

import com.cyxtera.wsOperaciones.model.Auditoria;


public interface AuditoriaService {

	/**
	 * Guarda auditoria
	 * @param auditoria
	 * @return Auditoria guardada
	 */
	Auditoria save(Auditoria auditoria);

	/**
	 * Recupera lista de auditoria
	 * @return lista de auditoria
	 */
	List<Auditoria> findAll();
	

	/**
	 * Elimina auditoria  por id
	 * @param id
	 */
	void deleteAuditoria(Long id);


}
