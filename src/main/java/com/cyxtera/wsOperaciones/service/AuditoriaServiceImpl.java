package com.cyxtera.wsOperaciones.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cyxtera.wsOperaciones.dao.AuditoriaRepository;
import com.cyxtera.wsOperaciones.model.Auditoria;

@Service
public class AuditoriaServiceImpl implements AuditoriaService {

	@Autowired
	protected AuditoriaRepository auditoriaRepository;

	@Override
	public Auditoria save(Auditoria auditoria) {
		return this.auditoriaRepository.save(auditoria);
	}

	@Override
	public List<Auditoria> findAll() {
	
		return this.auditoriaRepository.findAll();
	}

	@Override
	public void deleteAuditoria(Long id) {
		this.auditoriaRepository.deleteById(id);
	}
}
