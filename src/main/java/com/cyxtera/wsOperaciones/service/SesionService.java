package com.cyxtera.wsOperaciones.service;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.cyxtera.wsOperaciones.model.Sesion;

@Service
public class SesionService {
	
	public Sesion sesion;
	
	public SesionService() {
		this.sesion = new Sesion();
	}
	
	public void setSesion(String session) {
		this.sesion.setSession(session);
	}
	
	public String getSesion() {
		return this.sesion.getSession();
	}

}
