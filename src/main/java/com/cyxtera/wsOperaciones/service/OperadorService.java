package com.cyxtera.wsOperaciones.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class OperadorService {
private OperandoService operandoService;
private ArrayList<Double> valores;
	
	
	private Double resultado=0.0;
	private String res="No existe el operando digitado";
	
	
	public OperadorService() {
		this.operandoService = new OperandoService();
		valores =  this.operandoService.getOperandos();
	}

	public String realizaCalculo(String operador) {
		
		for (int i=0;i<valores.size();i++) {
			
			if (operador.equals("suma")) 
				resultado += valores.get(i);
			else if (operador.equals("resta"))
				resultado -= valores.get(i);
			else if (operador.equals("multiplicacion"))
				resultado *= valores.get(i);
			else if (operador.equals("division"))
				resultado /= valores.get(i);
			else if (operador.equals("potencia"))
				resultado = Math.pow(resultado, valores.get(i));
			
		}
		
		this.operandoService.limpiaOperandos();
		this.operandoService.addOperando(resultado.toString());
		res = resultado.toString();
		return res;
	}

}
