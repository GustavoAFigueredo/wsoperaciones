package com.cyxtera.wsOperaciones.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cyxtera.wsOperaciones.model.Auditoria;

public interface AuditoriaRepository extends JpaRepository<Auditoria, Long> {

	@SuppressWarnings("unchecked")
	Auditoria save(Auditoria auditoria);
	
}
