package com.cyxtera.wsOperaciones.model;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "auditoria")
@Access(AccessType.FIELD)
public class Auditoria extends ParentEntity {

	private static final long serialVersionUID = -3454836520926292366L;

	@Column(name = "sesion", nullable = false, length = 100)
	private String sesion;

	@Column(name = "evento", nullable = true, length = 255)
	private String evento;

	@Column(name = "fecha", nullable = false)
	private String fecha;

	public String getSesion() {
		return sesion;
	}

	public void setSesion(String sesion) {
		this.sesion = sesion;
	}

	public String getEvento() {
		return evento;
	}

	public void setEvento(String evento) {
		this.evento = evento;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	
	

}
