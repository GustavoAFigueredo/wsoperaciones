package com.cyxtera.wsOperaciones.model;

public class Operador {

	
	
	public Operador() {
		
	}
	
	public String sumar(Double valor1, Double valor2) {
		Double resultado = valor1 + valor2;
		return resultado.toString();
	}
	
	public String restar(Double valor1, Double valor2) {
		Double resultado = valor1 - valor2;
		return resultado.toString();
	}
	
	public String multiplicar(Double valor1, Double valor2) {
		Double resultado = valor1 * valor2;
		return resultado.toString();
	}
	
	public String dividir(Double valor1, Double valor2) {
		Double resultado = valor1 / valor2;
		return resultado.toString();
	}
	
	public String potenciar(Double valor1, Double valor2) {
		Double resultado = Math.pow(valor1,valor2);
		return resultado.toString();
	}
	
}
