package com.cyxtera.wsOperaciones.model;



public class Sesion {
	
	public String session;
	
	public Sesion() {
		this.session = null;
	}
	

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}
	
	

}
