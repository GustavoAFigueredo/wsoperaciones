package com.cyxtera.wsOperaciones.controllers;



import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cyxtera.wsOperaciones.service.OperandoService;

import com.cyxtera.wsOperaciones.util.RestResponse;


@RestController
public class OperandoController {

	public OperandoService operandoService;
	
	@RequestMapping(value = "/addOperando", method = RequestMethod.POST)
	public RestResponse nuevoOperando(@RequestParam String operando) {
		
		this.operandoService = new OperandoService();
		this.operandoService.addOperando(operando);
		return new RestResponse(HttpStatus.OK.value(), " Ok.");
	}

}
