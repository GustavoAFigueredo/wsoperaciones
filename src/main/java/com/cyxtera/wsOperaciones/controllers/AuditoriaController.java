package com.cyxtera.wsOperaciones.controllers;

import java.io.IOException;
import java.util.List;


import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cyxtera.wsOperaciones.model.Auditoria;
import com.cyxtera.wsOperaciones.service.AuditoriaService;
import com.cyxtera.wsOperaciones.util.RestResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;



@RestController
public class AuditoriaController {

	@Autowired
	protected AuditoriaService auditoriaService;

	protected ObjectMapper mapper;
	
	
	
	
	@RequestMapping(value = "/saveOrUpdate", method = RequestMethod.POST)
	public RestResponse saveOrUpdate(@RequestBody String auditJson)
			throws JsonParseException, JsonMappingException, IOException {
		
		this.mapper = new ObjectMapper();
		
		Auditoria auditoria = this.mapper.readValue(auditJson, Auditoria.class);

		if (!this.validate(auditoria)) {
			return new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(),
					"Los campos obligatorios no están diligenciados");
		}

		this.auditoriaService.save(auditoria);
		return new RestResponse(HttpStatus.OK.value(), " Operación exitosa.");
	}

	@RequestMapping(value = "/getAuditoria", method = RequestMethod.GET)
	public List<Auditoria> getAuditoria() {
		
		return this.auditoriaService.findAll();
		
	}
	
	@RequestMapping(value = "/deleteAuditoria", method = RequestMethod.POST)
	public void deleteAuditoria(@RequestBody String auditJson) throws Exception{
		this.mapper = new ObjectMapper();
		
		Auditoria auditoria = this.mapper.readValue(auditJson, Auditoria.class);
		
		if(auditoria.getId()==null) {
			throw new Exception("El id está nulo");
		}
		this.auditoriaService.deleteAuditoria(auditoria.getId());
	}
	private boolean validate(Auditoria auditoria) {
		boolean isValid = true;

		if (StringUtils.trimToNull(auditoria.getEvento()) == null) {
			isValid = false;
		}

		if (StringUtils.trimToNull(auditoria.getSesion()) == null) {
			isValid = false;
		}

		if (StringUtils.trimToNull(auditoria.getFecha()) == null) {
			isValid = false;
		}

		return isValid;
	}
}
