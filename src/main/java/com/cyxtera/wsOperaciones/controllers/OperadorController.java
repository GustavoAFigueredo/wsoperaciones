package com.cyxtera.wsOperaciones.controllers;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cyxtera.wsOperaciones.service.OperadorService;
import com.cyxtera.wsOperaciones.service.OperandoService;
import com.cyxtera.wsOperaciones.util.RestResponse;

@RestController
public class OperadorController {
	

	public OperadorService operadorService;
	
	
	@RequestMapping(value = "/realizaOperacion", method = RequestMethod.POST)
	public RestResponse nuevoOperador(@RequestParam String operador) {

		operadorService = new OperadorService();

	
		String resultado = this.operadorService.realizaCalculo(operador);

		return new RestResponse(HttpStatus.OK.value(), resultado);
	}

}
