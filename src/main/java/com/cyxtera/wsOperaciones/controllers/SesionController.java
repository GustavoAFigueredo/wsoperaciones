package com.cyxtera.wsOperaciones.controllers;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cyxtera.wsOperaciones.service.SesionService;

@RestController
public class SesionController {

protected String misession;
public SesionService sesionService;
	

@RequestMapping(value="/createSession")

public String login(HttpServletRequest request, HttpSession session) {
	
	this.sesionService = new SesionService();
	
	if (!session.isNew()) {
        session.invalidate();
    }
	
	
	misession = session.getId();
	this.sesionService.setSesion(misession);
    return misession;
}    

	
}
