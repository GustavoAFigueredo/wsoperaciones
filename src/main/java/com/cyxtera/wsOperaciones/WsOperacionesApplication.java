package com.cyxtera.wsOperaciones;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WsOperacionesApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsOperacionesApplication.class, args);
	}

}
